import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './Header.scss'

import { Project, Learn } from '../pages'

const FnVisible = () => {
    const [visible, setVisible] = useState(0)
    return {visible, setVisible}
}

const Header = () => {
    const Visible = FnVisible()
    const pathname =  window.location.pathname
    const firstPath = pathname.split('/')
    
    return (
        <div className='header-box'>
            <ul>
                <li onClick={() => {Visible.setVisible('home')}}>
                    <Link to='/'>KKu Island</Link>
                </li>
                <li onClick={() => {Visible.setVisible('about')}}>
                    <Link to='/about'>KKu About</Link>
                </li>
                <li onClick={() => {Visible.setVisible('learn')}}>
                    <Link to='/learn'>KKu Hook</Link>
                </li>
                <li onClick={() => {Visible.setVisible('project')}}>
                    <Link to='/project'>Mini Project</Link>
                </li>
                <li><a href='http://kkuisland.com:4000/'>PHP Home</a></li>
            </ul>
            {firstPath[1] === 'learn' && <Learn />}
            {firstPath[1] === 'project' && <Project />}
        </div>
    )
}

export default Header