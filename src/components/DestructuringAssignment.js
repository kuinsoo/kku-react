const array = ['dog', 'cat', 'sheep']
const [first, second] = array
console.log(first, second) // dog cat 

// 오브젝트 object 
const animals = {
    cat: "CAT",
    dog: "DOG",
    tiger: "TIGER"
}
