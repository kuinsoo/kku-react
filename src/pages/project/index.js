import React from 'react'
import { Link } from 'react-router-dom'

const index = () => {
    return (
        <div className='second_nav'>
            <p>리액트 훅을 이용한 미니 프로젝트</p>
            <ul>
                <li>▶ <Link to='/project/littlestar'>LittleStar</Link></li>
                <li>▶ <Link to='/project/todo'>Todo List</Link></li>
            </ul>
            <hr/>
        </div>
    )
}

export default index