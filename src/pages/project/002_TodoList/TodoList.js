import React from 'react'
import TodoTemplate from './TodoTemplate'
import TodoInsert from './TodoInsert'
import './TodoList.scss'

const TodoList = ({children}) => {
    return (
        <TodoTemplate>
            <TodoInsert />
        </TodoTemplate>
    )
}

export default TodoList
