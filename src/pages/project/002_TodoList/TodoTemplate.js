import React from 'react'

const TodoTemplate = ({children}) => {
    const paramAll = window.location.search
    const searchParams = new URLSearchParams(paramAll)
    const todoText = searchParams.get('todoText')

    return (
        <main className='todo-list-template'>
            <div className='todo-list-title'>오늘 할일</div>
            <section className='todo-insert'>{children}</section>
            <section className='todo-content'>{todoText}</section>
        </main>
    )
}

export default TodoTemplate
