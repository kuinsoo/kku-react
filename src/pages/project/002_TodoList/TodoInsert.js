import React, { useState, useRef, useCallback } from 'react'

const TodoInsert = () => {
    const [objTodo, setObjTodo] = useState([{
        id: 0,
        text: 'first',
        checked: false
    }])
    const nextId = useRef(1)
    const onInsert = useCallback(text => {
        const todo = {
            id: nextId,
            text,
            checked: false
        }
        setObjTodo(objTodo.concat(todo))
        nextId.current += 1
    },[objTodo])
    console.log(objTodo);
    console.log(nextId);
    console.log(onInsert);

    return (
        <form className='todo-list-insert'>
            <input name='todoText' placeholder='할 일을 입력하세요' />
            <button type='submit' onClick={(e) => {
                
                e.preventDefault()
            }}>+</button>
        </form>
    )
}

export default TodoInsert
