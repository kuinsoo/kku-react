import React, { useState } from 'react'
import './LittleStar.scss'

const LittleStar = () => {
    // const [visible, setVisible] = useState(false)
    const [startBox, setStarBox] = useState('')
    const [logState, setLogState] = useState('')
    const [number, setNumber] = useState(1)
    let globalLogBox = [];

    const onChangeNumber = e => {
        const curValue = e.target.value
        const newValue = curValue.replace(/[^1-9]/g,'')
        const lastValue = newValue.substring(0,1)
        setNumber(lastValue)
    }

    const drowStar = (number) => {
        let starBoxes = '';
        for (let i = 0; i < number; i++) {
            starBoxes += '*'
        }
        setStarBox(starBoxes)      
    }

    return (
        <div>
            <input value={number} onChange={onChangeNumber} placeholder="별이 그려질 수 입력" />
            <button onClick={() => {
                drowStar(number)
                globalLogBox.push(number + '개 별 생성')
                console.log(globalLogBox);
                
                setLogState(globalLogBox)
                // setVisible(!visible)
            }}>별 그리기</button>
            <button onClick={() => {
                setStarBox('')
                setNumber(1)
            }} >별 지우기</button>
            <hr />
            <div>
                <div className='star-box'>
                    <span>{ startBox }</span>
                </div>
                <div className='logo-box'>
                    { logState }
                </div>
            </div>
        </div>
    )
}

export default LittleStar