import React from 'react'

const aboutText = 
`
안녕하세요.\n
리액트를 공부하며 간단한 배포 환경까지 세팅하여 진행해 보고 있습니다.\n\n
서버: 라즈베리파이 4  -> Ubunt 1.8 -> Docker Container -> Node -> Create-React-App  간단하게 학습 용도로만 세팅\n
배포: 공부한 내용을 Gitlab 으로 push 해두면 서버는 10분마다 깃을 내려 받고 있는 중 이며 kkuisland.com  호스트를 구매 연결하여 어디서든 접속해 확인 가능\n
학습: React Hook 을 공부 중 이며 기존에 Class를 사용한 React 는 Function 방식으로 변경하는 정도만 익히고 학습하지 않음\n
비고: 학습한 내용을 바탕으로 1주일마다 미니 프로젝트를 수행하여 복습 및 실무에 좀 더 적응 되도록 하고 있습니다.\n
Gitlab: https://gitlab.com/kuinsoo/kku-react \n
Email: kkuinsoo@gmail.com  or kkuisland@gmail.com \n
`

const About = () => {
    const atext = aboutText
    return (
        <div>
            {
                atext.split('\n').map(line => {
                    return <span>{line}<br /></span>
                })
            }
        </div>
    )
}

export default About
