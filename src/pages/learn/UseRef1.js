import React, { useState, useMemo, useRef, useCallback }  from 'react'

const getAverage = numbers => {
    console.log('평균값 계산중..')
    if(numbers.length === 0) return 0
    const sum = numbers.reduce((a,b) => a + b)
    return sum / numbers.length
}

const Average = () => {
    const [list, setList] = useState([])
    const [number, setNumber] = useState('')
    const inputEl = useRef(null)

    const onChange = useCallback(e => {
        setNumber(e.target.value)
    }, [])

    const onInsert = useCallback(e => {
        const nextList = list.concat(parseInt(number))
        setList(nextList)
        setNumber('')
        inputEl.current.focus()
    },[number, list])

    const avg = useMemo(() => getAverage(list), [list])

    return {number, list, inputEl, onChange, onInsert, avg}
}

const UseRef1 = () => {
    const AV = Average()
    console.log(AV.list);
    
    return (
        <div>
            <p>
                useRef<br />
                함수형 컴포넌트에서 ref 를 쉽게 사용 할 수 있게 해줍니다.<br />
                useRef 를 통해 만든 객체 안의 current 값이 실제 엘리먼트를 가르키게 된다.
            </p>
            <hr />
            <input value={AV.number} onChange={AV.onChange} ref={AV.inputEl} />
            <button onClick={AV.onInsert}>등록</button>
            <ul>
                {AV.list.map((value, index) => (
                    <li key={index}>{value}</li>
                ))}
            </ul>
            <div>
                <b>평균 값:</b> {AV.avg}
            </div>
        </div>
    )
}

export default UseRef1
