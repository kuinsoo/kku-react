import React, { useEffect, useRef } from 'react'

const useClick = (onClick) => {
    const element = useRef()
    useEffect(() => {
        const ec = element.current
        console.log("DidMount");
        if(ec){
            ec.addEventListener('click', onClick)
        }
        return () => {
            console.log("willUnMount");
            if(ec) {
                ec.removeEventListener('click', onClick)
            }
        }
    }, [onClick])
    
    return element
}

const UseClick = () => {
    const focusRef = useRef() // === document.querySelector('#id)
    setTimeout(() => {
        focusRef.current.focus()
        console.log('포커스 이동')
    }, 2000)

    const clickLog = () => console.log('onClick Log')
    // const titleClick = useClick(log)
    const titleRef = useClick(clickLog)
    return (
        <div>
            <h1 ref={titleRef}>Title</h1>
            <div>useRef 2초 뒤에 포커스</div>
            <input ref={focusRef} placeholder="useRef" />
        </div>
    )
}

export default UseClick