import React, { createContext, useContext } from 'react'

const ThemeContext = createContext('black')
const explanation = createContext('이 Hook 을 사용하면 함수형 컴포넌트에서 Context 를 보다 쉽게 사용 할 수 있습니다.')

const ContextSample = () => {
    const theme = useContext(ThemeContext)
    const explain = useContext(explanation)
    const style = {
        width: '300px',
        height: '300px',
        color: 'white',
        background: theme
    }
    return <div style={style}>{explain}</div>
}
const UseContext = () => {
    return (
        <div>
            <p>
                
            </p>
            <ContextSample />
        </div>
    )
}

export default UseContext
