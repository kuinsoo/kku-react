import React , { useReducer } from 'react'

function reducer( state, action ) {
    console.log(action.name, 'action')
    console.log(state, 'state')
    return {
        ...state,
        [action.name]: action.value
    }
}

const UseReducer2 = () => {
    const [state, dispatch] = useReducer(reducer, {name:'', nickname:''})
    console.log('state', state)
    const {name, nickname} = state
    const onChange = e =>
    {
        dispatch(e.target)
    }
    return (
        <div>
            <p>useReducer : Input 상태 관리</p>
            <div>
                <input placeholder='name'       name='name'     value={name}        onChange={onChange} />
                <input placeholder='nickname'   name='nickname' value={nickname}    onChange={onChange} />
            </div>
            <div>
                <p><b>이름:</b>{name}</p>
                <p><b>닉네임:</b>{nickname}</p>
            </div>
        </div>
    )
}

export default UseReducer2
