import React, { useState, useEffect } from 'react'

const Info = () => {
    const [name, setName] = useState('구인수')

    const onChangeName = (e) => {
        setName(e.target.value)
    }

    useEffect(() => {
        console.log('effect')
        console.log(name)
        return () => {
            console.log('cleanup')
            console.log(name)
        }
    })

    return {name, onChangeName}
}

const UseEffect3 = () => {
    const [visible, setVisible] = useState(false)
    const infomation = Info()
    return (
        <div>
            <input onChange={infomation.onChangeName} />
            <button onClick={()=>{
                setVisible(!visible)
            }}>
                {visible ? '숨기기': '보이기'}
            </button>
            <hr />

            <p>
                {visible && infomation.name}
            </p>
        </div>
    )
}

export default UseEffect3