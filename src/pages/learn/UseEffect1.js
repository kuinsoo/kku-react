import React, { useState, useEffect } from 'react'

const arrContent = []
let NoOfIndex = 1
const LogBoard = (logText) => {
    const [logContent, setLogContent] = useState({
        index : 0,
        text : 'Log Start'
    })
    
    const addLog = () => {
        setLogContent({index:NoOfIndex, text:(`${NoOfIndex} ${logText}`)})
        arrContent.push(logContent)
        NoOfIndex++
        console.log(arrContent)
    }
    
    return {logContent, addLog}
}

const ComponentDidUpdate = () => {
    const [name, setName] = useState('KKu')
    const [number, setNumber] = useState(0)
    const onChangeName = e => {
        setName(e.target.value)
    }
    const onClickNumber = () => {
        setNumber(number+1)
    }
    return {name, onChangeName, number, onClickNumber}
}

const Info = () => {
    const [name, setName] = useState('')
    const [nickname, setNickname] = useState('')
    useEffect(() => {
        console.log('딱 한번만 렌더링이 됩니다.')
    }, [])

    const onChangeName = e => {
        setName(e.target.value)
    }

    const onChangeNickname = e => {
        setNickname(e.target.value)
    }
    return {name, nickname, onChangeName, onChangeNickname}
}

const UseEffect1 = () => {
    const didUpdate = ComponentDidUpdate()
    const infoUpdate = Info()
    const testtt = LogBoard('Log')
    
    
    useEffect(() => {
        console.log('componentDidMount')
        return () => {
            console.log('componentDidUpdate')
        }
    },[])
    return (
        <div>
            <div>useEffect!</div>
            <input onChange={didUpdate.onChangeName} />
            <div>{didUpdate.name}</div>
            <button onClick={didUpdate.onClickNumber}>+</button>
            <div>{didUpdate.number}</div>
            <hr />
            <p>Info Update</p>
            <input onChange={infoUpdate.onChangeName} />
            <p>{infoUpdate.name}</p>       
            <input onChange={infoUpdate.onChangeNickname} />
            <p>{infoUpdate.nickname}</p>
            <button onClick={testtt.addLog}>!!</button>
            <p>{testtt.logContent.text}</p>
        </div>
    )
}

export default UseEffect1
