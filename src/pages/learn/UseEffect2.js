import React, { useState, useEffect } from 'react'

let number = 1
let arrLogBox = []
const KKuLogBox = () => {
    const [logBox, setLogBox] = useState({index:0,text:'0 : Start Log'})
    
    const onClick = () => {
        setLogBox({index:number,text:`${number} : Start Log`})
        arrLogBox.push(logBox)
        number++
    }

    useEffect(() => {
        console.log('first ', logBox) // 현재값 
        return () => {
            console.log('return ',logBox) // 이전값 ( 값이 변경 되기 전에 실행 해야할 입무 )
        };
    })

    return {logBox, onClick}
}

const UseEffect2 = () => {
    const kkuLogBox = KKuLogBox()
    return (
        <div>
            <div>useEffect  cleanup</div>
            <button onClick={kkuLogBox.onClick}>렌더알림</button>
            {
                arrLogBox.map((data) => {
                    return (
                        <div key={data.index}>
                            {data.text}
                        </div>
                    )
                })}
        </div>
    )
}

export default UseEffect2