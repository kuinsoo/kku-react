import React from 'react'
import { Link } from 'react-router-dom'

const index = () => {
    return (
        <div className='second_nav'>
            <p>리액트 훅 친해지기</p>
            <ul>
                <li><Link to='/learn/counter'>counter</Link></li>
                <li><Link to='/learn/useinput'>userInput</Link></li>
                <li><Link to='/learn/useTabs'>useTabs</Link></li>
                <li><Link to='/learn/useEffect1'>useEffect1</Link></li>
                <li><Link to='/learn/useEffect2'>useEffect2</Link></li>
                <li><Link to='/learn/useEffect3'>useEffect3</Link></li>
                <br />
                <li><Link to='/learn/useTitle'>useTitle</Link></li>
                <li><Link to='/learn/useClick'>useClick</Link></li>
                <li><Link to='/learn/useConfirm'>useConfirm</Link></li>
                <li><Link to='/learn/useContext'>useContext</Link></li>
                <li><Link to='/learn/useReducer1'>useReducer1</Link></li>
                <li><Link to='/learn/useReducer2'>useReducer2</Link></li>
                <br />
                <li><Link to='/learn/useMemo'>useMemo</Link></li>
                <li><Link to='/learn/useCallback'>useCallback</Link></li>
                <li><Link to='/learn/useRef1'>UseRef1</Link></li>
                <li><Link to='/learn/useRef2'>UseRef2</Link></li>
                <li><Link to='/learn/customHook1'>CustomHook1</Link></li>
                <li><Link to='/learn/customHook2'>CustomHook1</Link></li>
                <br />
            </ul>
            <hr/>
        </div>
    )
}

export default index