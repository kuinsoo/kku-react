import React, { useReducer } from 'react'

function reducer(state, action) {
    // action.type 에 따라 다른 작업 수행
    switch (action.type)
    {
        case 'INCREMENT':
            return { value : state.value + 1 }
        case 'DECREMENT':
            return { value : state.value - 1 }
        default:
            // 아무것도 해당되지 않을 때 기존 상태 반환
            return state
    }
}

const UseReducer1 = () => {
    const [state, dispatch] = useReducer(reducer, {value: 0})
    const style = {
        background: 'gray'
    }
    return (
        <div>
            useReducer
            <p>현재 카운터 값은 <b>{state.value}</b> 입니다</p>
            <button onClick={() => dispatch({type: 'INCREMENT'})}>+1</button>
            <button onClick={() => dispatch({type: 'DECREMENT'})}>-1</button>
            <pre style={style}>
{`
function reducer(state, action) {
    // action.type 에 따라 다른 작업 수행
    switch (action.type)
    {
        case 'INCREMENT':
            return { value : state.value + 1 }
        case 'DECREMENT':
            return { value : state.value - 1 }
        default:
            // 아무것도 해당되지 않을 때 기존 상태 반환
            return state
    }
}

const UseReducer = () => {
    const [state, dispatch] = useReducer(reducer, {value: 0})
    return (
        <div>
            useReducer
            <p>현재 카운터 값은 <b>{state.value}</b> 입니다</p>
            <button onClick={() => dispatch({type: 'INCREMENT'})}>+1</button>
            <button onClick={() => dispatch({type: 'DECREMENT'})}>-1</button>
        </div>
    )
}
`}
            </pre>
            <p>
                useReducer 의 첫번째 파라미터는 리듀서 함수, 그리고 두번째 파라미터는 해당 리듀서의 기본 값을 넣어줍니다.<br/>이 Hook 을 사용 했을 때에는 state 값과 dispatch 함수를 받아오게 되는데요, 여기서 state 는 현재 가르키고 있는 상태고, dispatch 는 액션을 발생시키는 함수입니다.<br/>dispatch(action) 와 같은 형태로, 함수 안에 파라미터로 액션 값을 넣어주면 리듀서 함수가 호출되는 구조입니다.<br/>useReducer 을 사용했을 때의 가장 큰 장점은 컴포넌트 업데이트 로직을 컴포넌트 바깥으로 빼낼 수 있다는 점 입니다.
            </p>
        </div>
    )
}

export default UseReducer1
