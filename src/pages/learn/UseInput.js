import React, { useState } from 'react'

const useInput = (initalValue, validator) => {
    const [value, setValue] = useState(initalValue)
    const onChange = event => {
        const {
            target: { value }
        } = event
        let willUpdate = true
        if(typeof validator === 'function') {
            willUpdate = validator(value)
        }
        if(willUpdate) {
            setValue(value)
        }
    }
    return { value, onChange }
}

const UseInput = () => {
    const maxLen = (value) => value.length <= 10
    const name = useInput('Mr.', maxLen)
    return (
        <div className="useInputApp">
            <h1>10자 이상 사용금지 (validator 작성)</h1>
            <input placeholder="Name" {...name} />
        </div>
    )
}


export default UseInput