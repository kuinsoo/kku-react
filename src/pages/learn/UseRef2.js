import React, { useRef } from 'react'

const RefSample = () => {
    const id = useRef(1)
    const setId = (n) => {
        id.current = n
    }
    const printId = () => {
        console.log(id.current)
    }
    return {id, setId, printId}
}

const UseRef2 = () => {
    const refSample = RefSample()
    console.log(refSample)
    return (
        <div>
            <p>
                useRef2<br />
                주의 하실 점은, 이렇게 넣은 ref 안의 값은 바뀌어도 컴포넌트가 렌더링 되지 않는다는 점 입니다.<br />
                렌더링과 관련 되지 않은 값을 관리 할 때만 이러한 방식으로 코드 작성
            </p>
            <hr />
        </div>
    )
}

export default UseRef2
