import React, { useState, useEffect } from 'react'

const useTitle = initialTitle => {
    const [title, setTitle] = useState(initialTitle)
    const updateTitle = () => {
        const htmlTitle = document.querySelector('title')
        htmlTitle.innerText = title
    }
    useEffect(updateTitle, [title]) // componentDidMount,  compnentWillUpdate
    return setTitle
}

const UseTitle = () => {
    const titleUpdate = useTitle("Loading...")
    setTimeout(() => {
        titleUpdate('KKu Island')
    }, 5000)
    return (
        <div>타이틀 (Loading...)  5초 후  (KKu Island) 변경</div>
    )
}

export default UseTitle
