import React, { useState, useMemo, useCallback } from 'react'

const getAverage = numbers => {
    console.log('평균값 계산중..')
    if(numbers.length === 0) return 0
    const sum = numbers.reduce((a, b) => a + b )
    return sum / numbers.length
}

const Average = () => {
    const [list, setList] = useState([])
    const [number, setNumber] = useState('')

    const onChange = useCallback(e => {
        setNumber(e.target.value)
    }, []) // 컴퍼넌트가 처음 렌더링 될 때만 함수 생성 ( 이전값이 필요없이 비어 있어도 되는 상황 )

    const onInsert = useCallback(
        e => {
            const nextList = list.concat(parseInt(number))
            setList(nextList)
            setNumber('')
        }, [number, list]  // list 형태로 이전 값이 필요 한경우 
    )// number 또는 list 가 변경 되었을 때만 함수 실행
    const avg = useMemo(() => getAverage(list), [list])

    return {list, number, onChange, onInsert, avg}
}

const UseCallback = () => {
    const AVG = Average()
    return (
        <div>
            useCallback
            <input value={AVG.number} onChange={AVG.onChange} />
            <button onClick={AVG.onInsert}>등록</button>
            <ul>
                { AVG.list.map((value, index) => (
                    <li key={index}>{value}</li>
                ))}
            </ul>
            <div>
                <b>평균값:</b> {AVG.avg}
            </div>
            <p>
                useCallback 의 첫번째 파라미터에는 우리가 생성해주고 싶은 함수를 넣어주고,<br />
                두번째 파라미터에는 배열을 넣어 주면 되는데 이 배열에는 어떤 값이 바뀌었을 때 함수를 새로 생성해주어야 하는지 명시해주어야한다.<br />
                <br />
                만약 onChange 처럼 비어있는 배열을 넣게 되면 컴포넌트가 렌더링 될 때 단 한번만 함수가 생성되며, <br />
                onInsert 처럼 배열 안에 number 와  list 를 넣게 되면 인풋 내용이 바뀌거나 새로운 항목이 추가 될 때마다 함수가 생성됩니다.<br />
                <br />
                함수 내부에서 기존의 상태 값을 의존해야 할 때는 꼭 두번째 파라미터 안에 포함을 시켜주어야 한다.<br />
                예로 onChange 의 경우엔 기존의 값을 조회하는 이링 없고 바로 설정만 하기 때문에 배열이 비어있어도 상관없지만 <br />
                onInsert 는 기존의 number 와 list 조회에서 nextList 를 생성하기 때문에 배열 안에 nuber 와 list 를 꼭 넣어주어야한다.<br />
                <br />
                참고 아래는 같은 코드 
                <pre>
                {
`
useCallback(() => {
    console.log('hello world!')
},[])

useMemo(() => {
    const fn = () => {
        console.log('hello world!')
    }
    return fn
})

`
                }
                </pre>
                <br />
                useCallback 은 결국 useMemo 에서 함수를 반환하는 상황에서 더 편하게 사용 할 수 있는 Hook 이다.<br />
                숫자, 문자열, 객체 처럼 일반 값을 재사용하기 위해서는 useMemo 를,<br />
                그리고 함수를 재사용 하기 위해서는 useCallback 을 사용하면 된다.<br />

            </p>
        </div>
    )
}

export default UseCallback
