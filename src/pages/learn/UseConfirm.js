import React , { useState } from 'react'

const useConfirm = (message='', callback) => {
    const [msg, setMsg] = useState('메세지')
    const [style, setStyle] = useState({color: 'black'})
    if(typeof callback !== 'function') {
        return
    }

    const confirmAction = () => {
        if(window.confirm(message)) {
            callback()
            setMsg('console.log(Deleting the world...)')
            setStyle({color: 'red'})
        }
    }

    return { confirmAction, msg, style}
}

const UseConfirm = () => {
    const deleteWorld = () => console.log('Deleting the world...')
    const confirmDelete = useConfirm('Are you sure', deleteWorld)
    return (
        <div>
            <p>UseConfirm</p>
            <button onClick={confirmDelete.confirmAction}>Delete the World</button>
            <p style={confirmDelete.style}>{confirmDelete.msg}</p>
        </div>
    )
}

export default UseConfirm
