import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from '../components/Header'
import './App.scss'
import { Main, About, 
    Counter, UseInput, UseTabs, UseEffect1, UseEffect2, UseEffect3, UseTitle, UseClick, UseConfirm, 
    UseContext, UseReducer1, UseReducer2, UseMemo, UseCallback, UseRef1, UseRef2, CustomHook1, CustomHook2,
    LittleStar, TodoList } from '../pages'

const App = () => {
    return (
        
        <Router>
            <Header />
            <div className='app-box'>
                <Switch>
                    {/* 메인 라우터 */}
                    <Route path="/" exact={true} component={Main} />
                    {/* 메인 사이트 소개 */}
                    <Route path="/About" exact={true} component={About} />

                    {/* 학습관련 라우터 */}
                    <Route path="/learn/counter" exact={true} component={Counter} />
                    <Route path="/learn/useinput" exact={true} component={UseInput} />
                    <Route path="/learn/useTabs" exact={true} component={UseTabs} />
                    <Route path="/learn/useEffect1" exact={true} component={UseEffect1} />
                    <Route path="/learn/useEffect2" exact={true} component={UseEffect2} />
                    <Route path="/learn/useEffect3" exact={true} component={UseEffect3} />
                    <Route path="/learn/useTitle" exact={true} component={UseTitle} />
                    <Route path="/learn/useClick" exact={true} component={UseClick} />
                    <Route path="/learn/useConfirm" exact={true} component={UseConfirm} />
                    <Route path="/learn/useContext" exact={true} component={UseContext} />
                    <Route path="/learn/useReducer1" exact={true} component={UseReducer1} />
                    <Route path="/learn/useReducer2" exact={true} component={UseReducer2} />
                    <Route path="/learn/useMemo" exact={true} component={UseMemo} />
                    <Route path="/learn/useCallback" exact={true} component={UseCallback} />
                    <Route path="/learn/useRef1" exact={true} component={UseRef1} />
                    <Route path="/learn/useRef2" exact={true} component={UseRef2} />
                    <Route path="/learn/customHook1" exact={true} component={CustomHook1} />
                    <Route path="/learn/customHook2" exact={true} component={CustomHook2} />

                    {/* 프로젝트관련 라우터 */}
                    <Route path="/project/littlestar" exact={true} component={LittleStar} />
                    <Route path="/project/todo" exact={true} component={TodoList} />
                    
                </Switch>
            </div>
        </Router>
    )
}

export default App